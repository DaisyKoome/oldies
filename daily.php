 <?php

 //For the distance daily summary pie chart 

$query="SELECT distance FROM daily where saa='12pm'";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $pm12=$row['distance'];
        }
        //echo $pm12;
?>
<br>
<?php
$query="SELECT distance FROM daily where saa='6pm'";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $pm6=$row['distance'];
        }
       // echo $pm6;        
?>
<br>
<?php
$query="SELECT distance FROM daily where saa='12am'";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $am12=$row['distance'];
        }
        //echo $am12;        
?>
<br>
<?php
 
$dataPoints3 = array( 
        array("label"=>"12pm", "y"=>$pm12),
        array("label"=>"6pm", "y"=>$pm6),
        array("label"=>"12am", "y"=>$am12),
        
)
?>

<!-- End for daily distance -->

<!-- Beginning for daily blood pressure -->
<?php
$query="SELECT bp FROM daily where saa='12pm'";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $pm12_2=$row['bp'];
        }
        //echo $pm12_2;
?>
<br>
<?php
$query="SELECT bp FROM daily where saa='6pm'";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $pm6_2=$row['bp'];
        }
        //echo $pm6_2;        
?>
<br>
<?php
$query="SELECT bp FROM daily where saa='12am'";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $am12_2=$row['bp'];
        }
        //echo $am12_2;        
?>
<?php
 
$dataPoints4 = array( 
        array("label"=>"12pm", "y"=>$pm12_2),
        array("label"=>"6pm", "y"=>$pm6_2),
        array("label"=>"6am", "y"=>$am12_2),
        
)
?>

<!-- End for daily blood pressure -->