 <?php

 //For the distance weekly summary pie chart 

$query="SELECT distance FROM data where day='Monday'";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $mon=$row['distance'];
        }
        //echo $mon;
?>
<br>
<?php
$query="SELECT distance FROM data where day='Tuesday'";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $tue=$row['distance'];
        }
        //echo $tue;        
?>
<br>
<?php
$query="SELECT distance FROM data where day='Wednesday'";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $wed=$row['distance'];
        }
        //echo $wed;        
?>
<br>
<?php
$query="SELECT distance FROM data where day='Thursday'";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $thur=$row['distance'];
        }
        //echo $thur;        
?>
<br>
<?php
$query="SELECT distance FROM data where day='Friday'";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $fri=$row['distance'];
        }
        //echo $fri;        
?>
<br>

<?php
$query="SELECT distance FROM data where day='Saturday'";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $sat=$row['distance'];
        }
        //echo $sat;        
?>
<br>

<?php
$query="SELECT distance FROM data where day='Sunday'";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $sun=$row['distance'];
        }
        //echo $sun;        
?>
<br>
<?php
 
$dataPoints = array( 
	array("label"=>"Monday", "y"=>$mon),
	array("label"=>"Tuesday", "y"=>$tue),
	array("label"=>"Wednesday", "y"=>$wed),
	array("label"=>"Thursday", "y"=>$thur),
        array("label"=>"Friday", "y"=>$fri),
        array("label"=>"Saturday", "y"=>$sat),
        array("label"=>"Sunday", "y"=>$sun)
)
?>

 <!-- End of the distance weekly summary pie chart -->



<?php 
// For the blood pressure weekly summary pie chart

$query2="SELECT bp FROM data where day='Monday'";
        $result2=mysqli_query($con,$query2);
        
        while($row=mysqli_fetch_array($result2,MYSQLI_ASSOC)){
        $mon2=$row['bp'];
        }
        //echo $mon2;
?>
<br>
<?php
$query2="SELECT bp FROM data where day='Tuesday'";
        $result2=mysqli_query($con,$query2);
        
        while($row=mysqli_fetch_array($result2,MYSQLI_ASSOC)){
        $tue2=$row['bp'];
        }
       // echo $tue2;        
?>
<br>
<?php
$query2="SELECT bp FROM data where day='Wednesday'";
        $result2=mysqli_query($con,$query2);
        
        while($row=mysqli_fetch_array($result2,MYSQLI_ASSOC)){
        $wed2=$row['bp'];
        }
       // echo $wed2;        
?>
<br>
<?php
$query2="SELECT bp FROM data where day='Thursday'";
        $result2=mysqli_query($con,$query2);
        
        while($row=mysqli_fetch_array($result2,MYSQLI_ASSOC)){
        $thur2=$row['bp'];
        }
        //echo $thur2;        
?>
<br>
<?php
$query2="SELECT bp FROM data where day='Friday'";
        $result2=mysqli_query($con,$query2);
        
        while($row=mysqli_fetch_array($result2,MYSQLI_ASSOC)){
        $fri2=$row['bp'];
        }
        //echo $fri2;        
?>
<br>
<?php
$query2="SELECT bp FROM data where day='Saturday'";
        $result2=mysqli_query($con,$query2);
        
        while($row=mysqli_fetch_array($result2,MYSQLI_ASSOC)){
        $sat2=$row['bp'];
        }
        //echo $sat2;        
?>
<br>

<?php
$query2="SELECT bp FROM data where day='Sunday'";
        $result2=mysqli_query($con,$query2);
        
        while($row=mysqli_fetch_array($result2,MYSQLI_ASSOC)){
        $sun2=$row['bp'];
        }
        //echo $sun2;        
?>
<br>
<?php
 
$dataPoints2 = array( 
        array("label"=>"Monday", "y"=>$mon2),
        array("label"=>"Tuesday", "y"=>$tue2),
        array("label"=>"Wednesday", "y"=>$wed2),
        array("label"=>"Thursday", "y"=>$thur2),
        array("label"=>"Friday", "y"=>$fri2),
        array("label"=>"Saturday", "y"=>$sat2),
        array("label"=>"Sunday", "y"=>$sun2)
)
?>


 