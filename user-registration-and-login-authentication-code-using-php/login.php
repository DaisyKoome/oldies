<?php 
session_start();
include("DBConnect.php");
?>
<?php
$msg = ""; 
if(isset($_POST['submitBtnLogin'])) {
  $name = trim($_POST['name']);
  $idno = trim($_POST['idno']);
  if($name != "" && $idno != "") {
    try {
      $query = "SELECT * from `signup` where `name`=:name and `idno`=:idno";
      $stmt = $db->prepare($query);
      $stmt->bindParam('name', $name, PDO::PARAM_STR);
      $stmt->bindValue('idno', $idno, PDO::PARAM_STR);
      $stmt->execute();
      $count = $stmt->rowCount();
      $row   = $stmt->fetch(PDO::FETCH_ASSOC);
      if($count == 1 && !empty($row)) {
        /******************** Your code ***********************/
        $_SESSION['name']   = $row['name'];
        $_SESSION['idno'] = $row['idno'];
         header('location: ../pie.php');
       
      } else {
        $msg = "Invalid username and password!";
        echo $msg."<br><br>";
      }
    } catch (PDOException $e) {
      echo "Error : ".$e->getMessage();
    }
  } else {
    $msg = "Both fields are required!";
  }

}
?>
<!DOCTYPE html>
<html>
<head>
<title>Login</title>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Quicksand&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="styles.css">
<script>
    function loginvalidation(){
        var name = document.getElementById('name').value;
        var idno = document.getElementById('idno').value;

        var valid = true;

        if(name == ""){
        	   valid = false;
            document.getElementById('name_error').innerHTML = "required.";
        }

        if(idno == ""){
        	   valid = false;
            document.getElementById('idno_error').innerHTML = "required.";
        }
        return valid;
    }
    </script>
</head>
<body style="background: linear-gradient(to right, rgba(128, 128, 255, 1) 0%,rgba(255, 230, 247, 1) 50%);">
    <p style="font-size:40px; margin-top: 50px;">#Log in</p>
    <div class="demo-content" style="margin-top: 50px; background:linear-gradient(to left, rgba(128, 128, 255, 1) 0%,rgba(255, 230, 247, 1) 50%);">
        <form action="" method="POST"
            onsubmit="return loginvalidation();">


            <div class="row">
                <p style="font-size: 15px;">Name</p> <span id="name_error"></span>
                <div>
                    <input type="text" name="name" id="name"
                        class="form-control"
                        placeholder="Enter your full name">
                </div>
            </div>

            <div class="row" >
                <p style="font-size: 15px;">ID <br>number</p><span id="idno_error"></span>
                <div>
                    <input type="text" name="idno" id="idno"
                        class="form-control"
                        placeholder="Enter your ID number">

                </div>
            </div>
            <div class="row">
                <div>
                    <input type="submit" name="submitBtnLogin" id="submitBtnLogin" value="Login" style="height: 40px; width: 80px; margin-left: 100px; background-color: #c284e1; border:none; color: white; font-size: 15px;"/>
                </div>
            </div>
            <div class="row">
                <div>
                    <a href="index.php"><button type="button"
                            name="submit" class="btn signup" style="height: 40px; width: 80px; margin-left: 100px;background-color: #c284e1; border:none; font-size: 15px;">Signup</button></a>
                </div>
            </div>
        </form>
    </div>
</body>
</html>