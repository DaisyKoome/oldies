<?php
if($_POST){
 
    // include database connection
    include 'DBConnect.php';
 
    try{
     
        // insert query
        $query = "INSERT INTO signup SET name=:name, idno=:idno, docemail=:docemail, famemail=:famemail";
 
        // prepare query for execution
        $stmt = $db->prepare($query);
 
        // posted values
        $name=htmlspecialchars(strip_tags($_POST['name']));
        $idno=htmlspecialchars(strip_tags($_POST['idno']));
        $docemail=htmlspecialchars(strip_tags($_POST['docemail']));
        $famemail=htmlspecialchars(strip_tags($_POST['famemail']));
 

        // bind the parameters
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':idno', $idno);
        $stmt->bindParam(':docemail', $docemail);
        $stmt->bindParam(':famemail', $famemail);
         
         
        // Execute the query
        if($stmt->execute()){
            echo "<div class='alert alert-success'>Record was saved.</div>";
        }else{
            echo "<div class='alert alert-danger'>Unable to save record.</div>";
        }
         
    }
     
    // show error
    catch(PDOException $exception){
        die('ERROR: ' . $exception->getMessage());
    }
}


?>
<!DOCTYPE html>
<html>
<head>
<title>Sign Up</title>
<link rel="stylesheet" type="text/css" href="styles.css">
<script>
    function signupvalidation(){
        var name = document.getElementById('name').value;
        var idno = document.getElementById('idno').value;
        var docemail = document.getElementById('docemail').value;
        var famemail = document.getElementById('famemail').value;
        var emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
    	
        var valid=true;

        if(name == ""){
            valid = false;
            document.getElementById('name_error').innerHTML = "required.";
        }

        if(idno == ""){
            valid = false;
            document.getElementById('idno_error').innerHTML = "required.";
        }

        if(docemail == ""){
            valid = false;
            document.getElementById('docemail_error').innerHTML = "required.";
        } else {
            if(!emailRegex.test(docemail)){
                valid = false;
                document.getElementById('docemail_error').innerHTML = "invalid.";
            }
        }

        if(famemail == ""){
            valid = false;
            document.getElementById('famemail_error').innerHTML = "required.";
        } else {
            if(!emailRegex.test(famemail)){
                valid = false;
                document.getElementById('famemail_error').innerHTML = "invalid.";
            }
        }
    }
    </script>
</head>
<body style="background: linear-gradient(to right, rgba(128, 128, 255, 1) 0%,rgba(255, 230, 247, 1) 50%);">
    <p style="font-size: 40px;">#Sign up</p>
    <div class="demo-content" style="background:linear-gradient(to left, rgba(128, 128, 255, 1) 0%,rgba(255, 230, 247, 1) 50%);">
        <?php
        if (! empty($response)) {
            ?>
        <div id="response" class="<?php echo $response["type"]; ?>"><?php echo $response["message"]; ?></div>
        <?php
        }
        ?>
        <form action="" method="POST"
            onsubmit="return signupvalidation()">
            <div class="row">
                <label>Name</label><span id="name_error"></span>
                <div>
                    <input type="text" class="form-control" name="name"
                        id="name" placeholder="Enter your name">

                </div>
            </div>

            <div class="row">
                <label>ID number</label><span id="idno_error"></span>
                <div>
                    <input type="text" name="idno" id="idno"
                        class="form-control"
                        placeholder="Enter your ID number">

                </div>
            </div>

            <div class="row">
                <label>Doctor's email</label><span id="docemail_error"></span>
                <div>
                    <input type="email" name="docemail" id="docemail"
                        class="form-control"
                        placeholder="Enter your doctor's email">

                </div>
            </div>

            <div class="row">
                <label>Family member's email</label><span
                    id="famemail_error"></span>
                <div>
                    <input type="text" name="famemail"
                        id="famemail" class="form-control"
                        placeholder="Enter a trusted family member's email">

                </div>
            </div>


            <div class="row">
                <div align="center">
                    <button type="submit" name="submit"
                        class="btn signup"
                        style="height: 40px; width: 80px; margin-left: 5px; background-color: #c284e1; border:none; color: white;">Sign Up</button>
                </div>
            </div>

            <div class="row">
                <div>
                    <a href="login.php"><button type="button" name="submit"
                        class="btn login"
                        style="height: 40px; width: 80px; margin-left: 100px; background-color: #c284e1; border:none; color: white;">Login</button></a>
                </div>
            </div>
    
    </div>

    </form>
    </div>
</body>
</html>