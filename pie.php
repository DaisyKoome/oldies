<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Quicksand&display=swap" rel="stylesheet">

  <style type="text/css">
    .p1{
  background: linear-gradient(to left, rgba(71, 71, 209, 1) -20%, rgba(255, 77, 255, 1) 80%);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
    }
  </style>
</head>
<body style=" margin-top: 0px; padding-top: 0px; background: linear-gradient(to right, rgba(128, 128, 255, 1) 0%,rgba(255, 230, 247, 1) 50%);">
<p style="font-family: 'Quicksand', sans-serif; margin-left: 5%; margin-top: 200px; font-size: 100px; text-align: center; 
color:linear-gradient(to left, rgba(128, 128, 255, 1) 0%,rgba(255, 230, 247, 1) 50%); " class="p1"> The Exercise Assistant</p>
<img src="down.jpg" height="150px" width="100px" style="margin-top: 50px; margin-left: 45%;
background: linear-gradient(to right, rgba(128, 128, 255, 1) 0%,rgba(255, 230, 247, 1) 50%);">
<?php
require('db.php');
require('weekly_data.php');
require('daily.php');
?>
<style type="text/css">
html{
        margin-top: 0px;
        padding-top: 0px;
}
</style>
<script>
window.onload = function() {

var chart4 = new CanvasJS.Chart("chartContainer4", {
        theme: "light2",
        animationEnabled: true,
        title: {
                text: "Blood pressure daily summary",
                padding:"0px"
        },
        data: [{
                type: "pie",
                indexLabel: "{y}",
                yValueFormatString: "#,##0\"mmHg\"",
                indexLabelPlacement: "inside",
                indexLabelFontColor: "#36454F",
                indexLabelFontSize: 18,
                indexLabelFontWeight: "bolder",
                showInLegend: true,
                legendText: "{label}",
                dataPoints: <?php echo json_encode($dataPoints4, JSON_NUMERIC_CHECK); ?>
        }]
});
chart4.render();        
 
var chart3 = new CanvasJS.Chart("chartContainer3", {
        theme: "light2",
        animationEnabled: true,
        title: {
                text: "Distance daily summary",
                padding:"0px"
        },
        data: [{
                type: "pie",
                indexLabel: "{y}",
                yValueFormatString: "#,##0\"metres\"",
                indexLabelPlacement: "inside",
                indexLabelFontColor: "#36454F",
                indexLabelFontSize: 18,
                indexLabelFontWeight: "bolder",
                showInLegend: true,
                legendText: "{label}",
                dataPoints: <?php echo json_encode($dataPoints3, JSON_NUMERIC_CHECK); ?>
        }]
});
chart3.render();


var chart = new CanvasJS.Chart("chartContainer", {
        theme: "light2",
        animationEnabled: true,
        title: {
                text: "Distance walked weekly summary"
        },
        data: [{
                type: "pie",
                indexLabel: "{y}",
                yValueFormatString: "#,##0\"metres\"",
                indexLabelPlacement: "inside",
                indexLabelFontColor: "#36454F",
                indexLabelFontSize: 18,
                indexLabelFontWeight: "bolder",
                showInLegend: true,
                legendText: "{label}",
                dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
        }]
});
chart.render();

var chart2 = new CanvasJS.Chart("chartContainer2", {
        theme: "light2",
        animationEnabled: true,
        title: {
                text: "Blood pressure weekly summary"
        },
        data: [{
                type: "pie",
                indexLabel: "{y}",
                yValueFormatString: "#,##0\"mmHg\"",
                indexLabelPlacement: "inside",
                indexLabelFontColor: "#36454F",
                indexLabelFontSize: 18,
                indexLabelFontWeight: "bolder",
                showInLegend: true,
                legendText: "{label}",
                dataPoints: <?php echo json_encode($dataPoints2, JSON_NUMERIC_CHECK); ?>
        }]
});
chart2.render();

}    

</script>
<table>
<tr>
<td>
<div id="chartContainer4" style="height: 450px; width: 800px; margin-left: 0px;
margin-top: 0px; padding-top: 0px;display: inline-block;">
</div>
</td>
<td>
 <div class="container"><p>Normal Blood Pressure(BP): 140mmHg</p><br>
        <p>At 12pm</p>
  <div class="progress">
    
      <?php

if ($pm12_2 < 139) {
     echo "<div class='progress-bar bg-danger' style='width:33.33%''>".
                "Low BP"."</div>";
} elseif ($pm12_2=139 || $pm12_2=140 || $pm12_2=141) {
     echo "<div class='progress-bar bg-success' style='width:66.66%'>".
     "Normal"."</div>";
} else {
    echo "<div class='progress-bar bg-warning' style='width:100%'>".
    "Low BP"."</div>";
}
?>
    </div>
</div>
<div class="container"><br><p>At 6pm</p>
  <div class="progress">
    
      <?php

if ($pm6_2 < 139) {
     echo "<div class='progress-bar bg-danger' style='width:33.33%''>".
                "Low BP"."</div>";
} elseif ($pm6_2==139 || $pm6_2==140 || $pm6_2==141) {
     echo "<div class='progress-bar bg-success' style='width:66.66%'>".
     "Normal"."</div>";
} elseif ($pm6_2>141) {
    echo "<div class='progress-bar bg-warning' style='width:100%'>".
    "High BP"."</div>";
}
?>
    </div> 
  </div>
<br>
<div class="container"><p>At 12am</p>
  <div class="progress">
    <?php

if ($am12_2 < 139) {
     echo "<div class='progress-bar bg-danger' style='width:33.33%''>".
                "Low BP"."</div>";
} elseif ($am12_2=139 || $am12_2=140 || $am12_2=141) {
     echo "<div class='progress-bar bg-success' style='width:66.66%'>".
     "Normal"."</div>";
} else {
    echo "<div class='progress-bar bg-warning' style='width:100%'>".
    "Low BP"."</div>";
}
?>
  </div>
</div>
</td>
<td>
<?php

if ($am12_2 < 139) {
     echo "<div class='btn btn-outline-warning'>"."At 12am: Your blood pressure is"."<strong>"." low"."</strong>"."</div>"."<br>";
}
if ( $pm12_2 < 139) {
     echo "<div class='btn btn-outline-warning'>"."At 12pm: Your blood pressure is"."<strong>"." low"."</strong>"."</div>"."<br>";
}
if ($pm6_2 < 139) {
     echo "<div class='btn btn-outline-warning'>"."At 6am: Your blood pressure is"."<strong>"." low"."</strong>"."</div>"."<br>";
}
?>
<?php
if ($am12_2 > 141) {
      echo "<br>"."<div class='btn btn-outline-danger'>"."At 12am: Your blood pressure is"."<strong>"." high"."</strong>"."</div>"."<br>";
}
if ($pm12_2 > 141) {
      echo "<br>"."<div class='btn btn-outline-danger'>"."At 12pm: Your blood pressure is"."<strong>"." high"."</strong>"."</div>"."<br>";
}
if ($pm6_2 > 141) {
      echo "<br>"."<div class='btn btn-outline-danger'>"."At 6pm: Your blood pressure is"."<strong>"." high"."</strong>"."</div>".
       "<br>";
}
?>
<?php
if ($am12_2 ==141){
    echo "<br>"."<div class='btn btn-outline-success'>"."At 12am: Your blood pressure is"."<strong>"." normal"."</strong>"."</div>"."<br>";
}
elseif ($am12_2 == 140){
    echo "<br>"."<div class='btn btn-outline-success'>"."At 12am: Your blood pressure is"."<strong>"." normal"."</strong>"."</div>"."<br>";
}
elseif ($am12_2 == 139){
    echo "<br>"."<div class='btn btn-outline-success'>"."At 12am: Your blood pressure is"."<strong>"." normal"."</strong>"."</div>" ."<br>";
}
if ($pm12_2 == 139){
    echo "<br>"."<div class='btn btn-outline-success'>"."At 12pm: Your blood pressure is"."<strong>"." normal"."</strong>"."</div>" ."<br>";
}
elseif ($pm12_2 == 141){
    echo "<br>"."<div class='btn btn-outline-success'>"."At 12pm: Your blood pressure is"."<strong>"." normal"."</strong>"."</div>" ."<br>";
}
elseif ($pm12_2 == 140){
    echo "<br>"."<div class='btn btn-outline-success'>"."At 12pm: Your blood pressure is"."<strong>"." normal"."</strong>"."</div>" ."<br>";
}
if ($pm6_2 ==139){
    echo "<br>"."<div class='btn btn-outline-success'>"."At 6pm: Your blood pressure is"."<strong>"." normal"."</strong>"."</div>"."<br>";
}
elseif ($pm6_2 == 140){
    echo "<br>"."<div class='btn btn-outline-success'>"."At 6pm: Your blood pressure is"."<strong>"." normal"."</strong>"."</div>"  ."<br>";
}
elseif ($pm6_2 == 141 ){
    echo "<br>"."<div class='btn btn-outline-success'>"."At 6pm: Your blood pressure is"."<strong>"." normal"."</strong>"."</div>"."<br>";
}
?>
</td>

</tr>

<br><br>
<tr>
<td>        
<div id="chartContainer3" style="height: 450px; width: 800px; margin-left: 0px;
margin-top: 50px; padding-top: 0px;display: inline-block;"></div><br>
</td>
<td> 
 <div class="container"><p>Daily target: 600m</p>
        <br>
        <p>At 12pm</p>
  <div class="progress">
    
      <?php

if ($pm12 <= 200) {
     echo "<div class='progress-bar bg-danger' style='width:33.33%''>".
                "Keep going!"."</div>";
} elseif ($pm12 > 200 && $pm12<=400) {
     echo "<div class='progress-bar bg-warning' style='width:66.66%'>".
     "A little bit more..."."</div>";
} else {
    echo "<div class='progress-bar bg-success' style='width:100%'>".
    "Well done!"."</div>";
}
?>
    </div>  
  </div>

  <div class="container"><br><p>At 6pm</p>
  <div class="progress">
    
      <?php

if ($pm6 <= 200) {
     echo "<div class='progress-bar bg-danger' style='width:33.33%''>".
                "Keep going!"."</div>";
} elseif ($pm6 > 200 && $pm6<=400) {
     echo "<div class='progress-bar bg-warning' style='width:66.66%'>".
     "A little bit more..."."</div>";
} else {
    echo "<div class='progress-bar bg-success' style='width:100%'>".
    "Well done!"."</div>";
}
?>
    </div>  
  </div>

  <div class="container"><br><p>At 12am</p>
  <div class="progress">
    
      <?php

if ($am12 <= 200) {
     echo "<div class='progress-bar bg-danger' style='width:33.33%''>".
                "Keep going!"."</div>";
} elseif ($am12 > 200 && $am12<=400) {
     echo "<div class='progress-bar bg-warning' style='width:66.66%'>".
     "A little bit more..."."</div>";
} else {
    echo "<div class='progress-bar bg-success' style='width:100%'>".
    "Well done!"."</div>";
}
?>
    </div>  
  </div>
</td>


<td>
<?php

if ($am12< 201) {
     echo "<div class='btn btn-outline-warning'>"."At 12am: Keep going"."</div>"."<br>";
}
if ( $pm12 <201) {
     echo "<div class='btn btn-outline-warning'>"."At 12pm: Keep going "."</div>"."<br>";
}
if ($pm6<201) {
     echo "<div class='btn btn-outline-warning'>"."At 6am: Keep going"."</div>"."<br>";
}
?>
<?php
if ($am12 >200 && $am12< 401) {
      echo "<br>"."<div class='btn btn-outline-danger'>"."At 12am: A liitle bit more..."."</div>"."<br>";
}
if ($pm12 >200 && $pm12< 401) {
      echo "<br>"."<div class='btn btn-outline-danger'>"."At 12pm: A liitle bit more..."."</div>"."<br>";
}
if ($pm6 >200 && $pm6< 401) {
      echo "<br>"."<div class='btn btn-outline-danger'>"."At 6pm: A liitle bit more..."."</div>".
       "<br>";
}
?>
<?php
if ($am12>400){
    echo "<br>"."<div class='btn btn-outline-success'>"."At 12am: Done!"."</div>"."<br>";
}
elseif ($pm12>400){
    echo "<br>"."<div class='btn btn-outline-success'>"."At 12am: Done!"."</div>"."<br>";
}
elseif ($pm6>400){
    echo "<br>"."<div class='btn btn-outline-success'>"."At 12am: Done!"."</div>" ."<br>";
}

?>
</td>



</tr>
<br><br>
<tr>
<td>        
<div id="chartContainer" style="height: 450px; width: 800px; margin-left: 0px;
margin-top: 50px; padding-top: 0px; display: inline-block;"></div><br>
</td>
<td>
<div class="container">Daily target: 600m
        <br>
        <p>Monday</p>
  <div class="progress">
    
      <?php

if ($mon <= 200) {
     echo "<div class='progress-bar bg-danger' style='width:33.33%'>".
                "Keep going!"."</div>";
} elseif ($mon > 200 && $mon<=400) {
     echo "<div class='progress-bar bg-warning' style='width:66.66%'>".
     "A little bit more!"."</div>";
} else {
    echo "<div class='progress-bar bg-success' style='width:100%'>".
    "Well done!"."</div>";
}
?>
    </div>  
  </div>

  <div class="container"><p>Tuesday</p>
  <div class="progress">
    
      <?php

if ($tue <= 200) {
     echo "<div class='progress-bar bg-danger' style='width:33.33%''>".
                "Keep going!"."</div>";
} elseif ($tue > 200 && $tue<=400) {
     echo "<div class='progress-bar bg-warning' style='width:66.66%'>".
     "A little bit more..."."</div>";
} else {
    echo "<div class='progress-bar bg-success' style='width:100%'>".
    "Well done!"."</div>";
}
?>
    </div>  
  </div>

  <div class="container"><p>Wednesday</p>
  <div class="progress">
    
      <?php

if ($wed <= 200) {
     echo "<div class='progress-bar bg-danger' style='width:33.33%''>".
                "Keep going!"."</div>";
} elseif ($wed > 200 && $wed<=400) {
     echo "<div class='progress-bar bg-warning' style='width:66.66%'>".
     "A little bit more..."."</div>";
} else {
    echo "<div class='progress-bar bg-success' style='width:100%'>".
    "Well done!"."</div>";
}
?>
    </div>  
  </div>

<div class="container"><p>Thursday</p>
  <div class="progress">
    
      <?php

if ($thur <= 200) {
     echo "<div class='progress-bar bg-danger' style='width:33.33%''>".
                "Keep going!"."</div>";
} elseif ($thur > 200 && $thur<=400) {
     echo "<div class='progress-bar bg-warning' style='width:66.66%'>".
     "A little bit more..."."</div>";
} else {
    echo "<div class='progress-bar bg-success' style='width:100%'>".
    "Well done!"."</div>";
}
?>
    </div>  
  </div>

  <div class="container"><p>Friday</p>
  <div class="progress">
    
      <?php

if ($fri <= 200) {
     echo "<div class='progress-bar bg-danger' style='width:33.33%''>".
                "Keep going"."</div>";
} elseif ($fri > 200 && $fri<=400) {
     echo "<div class='progress-bar bg-warning' style='width:66.66%'>".
     "A little bit more..."."</div>";
} else {
    echo "<div class='progress-bar bg-success' style='width:100%'>".
    "Well done!"."</div>";
}
?>
    </div>  
  </div>

  <div class="container"><p>Saturday</p>
  <div class="progress">
    
      <?php

if ($sat <= 200) {
     echo "<div class='progress-bar bg-danger' style='width:33.33%''>".
                "Keep going!"."</div>";
} elseif ($sat > 200 && $sat<=400) {
     echo "<div class='progress-bar bg-warning' style='width:66.66%'>".
     "A little bit more..."."</div>";
} else {
    echo "<div class='progress-bar bg-success' style='width:100%'>".
    "Well done!"."</div>";
}
?>
    </div>  
  </div>

  <div class="container"><p>Sunday</p>
  <div class="progress">
    
      <?php

if ($sun <= 200) {
     echo "<div class='progress-bar bg-danger' style='width:33.33%''>".
                "Keep going!"."</div>";
} elseif ($sun > 200 && $sun<=400) {
     echo "<div class='progress-bar bg-warning' style='width:66.66%'>".
     "A little bit more..."."</div>";
} else {
    echo "<div class='progress-bar bg-success' style='width:100%'>".
    "Well done!"."</div>";
}
?>
    </div>  
  </div>

</td>
</tr>
<br><br>
<tr>
<td>                
<div id="chartContainer2" style="height: 450px; width: 800px; margin-left: 0px;
margin-top: 50px; padding-top: 0px; display: inline-block;"></div>
</td>
<td>
<div class="container">Normal Blood Pressure(BP): 140mmHg<br>
        <p>Monday</p>
  <div class="progress">
    
      <?php

if ($mon2 < 139) {
     echo "<div class='progress-bar bg-danger' style='width:33.33%''>".
                "Low BP"."</div>";
} elseif ($mon2=139 || $mon2=140 || $mon2=141) {
     echo "<div class='progress-bar bg-success' style='width:66.66%'>".
     "Normal"."</div>";
} else {
    echo "<div class='progress-bar bg-warning' style='width:100%'>".
    "Low BP"."</div>";
}
?>
    </div>
</div>

<div class="container"><p>Tuesday</p>
  <div class="progress">
    
      <?php

if ($tue2 < 139) {
     echo "<div class='progress-bar bg-danger' style='width:33.33%''>".
                "Low BP"."</div>";
} elseif ($tue2==139 || $tue2==140 || $tue2==141) {
     echo "<div class='progress-bar bg-success' style='width:66.66%'>".
     "Normal"."</div>";
} elseif ($tue2>141) {
    echo "<div class='progress-bar bg-warning' style='width:100%'>".
    "High BP"."</div>";
}
?>
    </div> 
  </div>

<div class="container"><p>Wednesday</p>
  <div class="progress">
    
      <?php

if ($wed2 < 139) {
     echo "<div class='progress-bar bg-danger' style='width:33.33%''>".
                "Low BP"."</div>";
} elseif ($wed2==139 || $wed2==140 || $wed2==141) {
     echo "<div class='progress-bar bg-success' style='width:66.66%'>".
     "Normal"."</div>";
} elseif ($wed2>141) {
    echo "<div class='progress-bar bg-warning' style='width:100%'>".
    "High BP"."</div>";
}
?>
    </div> 
  </div>

<div class="container"><p>Thursday</p>
  <div class="progress">
    
      <?php

if ($thur2 < 139) {
     echo "<div class='progress-bar bg-danger' style='width:33.33%''>".
                "Low BP"."</div>";
} elseif ($thur2==139 || $thur2==140 || $thur2==141) {
     echo "<div class='progress-bar bg-success' style='width:66.66%'>".
     "Normal"."</div>";
} elseif ($thur2>141) {
    echo "<div class='progress-bar bg-warning' style='width:100%'>".
    "High BP"."</div>";
}
?>
    </div> 
  </div>

<div class="container"><p>Friday</p>
  <div class="progress">
    
      <?php

if ($fri2 < 139) {
     echo "<div class='progress-bar bg-danger' style='width:33.33%''>".
                "Low BP"."</div>";
} elseif ($fri2==139 || $fri2==140 || $fri2==141) {
     echo "<div class='progress-bar bg-success' style='width:66.66%'>".
     "Normal"."</div>";
} elseif ($fri2>141) {
    echo "<div class='progress-bar bg-warning' style='width:100%'>".
    "High BP"."</div>";
}
?>
    </div> 
  </div>

<div class="container"><p>Saturday</p>
  <div class="progress">
    
      <?php

if ($sat2 < 139) {
     echo "<div class='progress-bar bg-danger' style='width:33.33%''>".
                "Low BP"."</div>";
} elseif ($sat2==139 || $sat2==140 || $sat2==141) {
     echo "<div class='progress-bar bg-success' style='width:66.66%'>".
     "Normal"."</div>";
} elseif ($sat2>141) {
    echo "<div class='progress-bar bg-warning' style='width:100%'>".
    "High BP"."</div>";
}
?>
</div>
</div>
<div class="container"><p>Sunday</p>
  <div class="progress">
    
      <?php

if ($sun2 < 139) {
     echo "<div class='progress-bar bg-danger' style='width:33.33%''>".
                "Low BP"."</div>";
} elseif ($sun2==139 || $sun2==140 || $sun2==141) {
     echo "<div class='progress-bar bg-success' style='width:66.66%'>".
     "Normal"."</div>";
} elseif ($sun2>141) {
    echo "<div class='progress-bar bg-warning' style='width:100%'>".
    "High BP"."</div>";
}
?>
    </div> 
  </div>

</td>
</tr>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</table>
<div>
  <br><p style="font-size: 30px;">#Google map</p>
  <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d15955.340542692313!2d36.81425975!3d-1.2720012499999998!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2ske!4v1574054984592!5m2!1sen!2ske" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</div>
</body>
</html>                              