        <?php
            // include database connection
            require('db.php');
            

            // end of delete message prompt
            
            //fname=:fname, mname=:mname, lname=:lname, nid=:nid, email=:email, pno=:pno, dept=:dept, role=:role, uname=:uname, psw=:psw
            // select all data
            $query = "SELECT day, distance, bp FROM data";
            $stmt = $con->prepare($query);
            $stmt->execute();
             
            // this is how to get number of rows returned
            $num = $stmt->rowCount();
            
            //check if more than 0 record found
            if($num>0){
             
                // data from database will be here

                echo "<table class='table table-hover table-responsive table-bordered' style='width:91%; margin-left:4.5%; margin-right:4.5%; font-size:14px;'>";//start table
                
                //id, fname, mname, lname, nid, email, pno, dept, role, uname, psw
                //creating our table heading
                echo "<tr>";
                    echo "<th>Day</th>";
                    echo "<th>Distance</th>";
                    echo "<th>Blood_Pressure</th>";
                echo "</tr>";
            
                // table body will be here

                // retrieve our table contents
                // fetch() is faster than fetchAll()
                // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    // extract row
                    // this will make $row['firstname'] to
                    // just $firstname only
                    extract($row);
                    
                    //id, fname, mname, lname, nid, email, pno, dept, role, uname, psw
                    // creating new table row per record
                    echo "<tr>";
                    echo "<td>{$day}</td>";
                    echo "<td>{$distance}</td>";
                    echo "<td>{$bp}</td>";
                    echo "<td>";
                    
    }
}
    
$dataPoints = array(
    array("y" => 543, "label" => "Sunday"),
    array("y" => 158, "label" => "Monday"),
    array("y" => 259, "label" => "Tuesday"),
    array("y" => 435, "label" => "Wednesday"),
    array("y" => 290, "label" => "Thursday"),
    array("y" => 657, "label" => "Friday"),
    array("y" => 843, "label" => "Saturday")
);
 
?>